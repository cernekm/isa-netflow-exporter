#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
__author__ = "Martin Cernek"
__email__ = "xcerne01@stud.fit.vutbr.cz"
"""

_HELP = """Usage:
isa_exporter [-i <file>] [-c <netflow_collector>[:<port>]] [-I <interval>] [-m <count>] [-t <seconds>]\n
-i [--input] <file> name of file
-c [--collector] <neflow_collector:port> IPv4 address or hostname with UDP port of NetFlow collector
-I [--interval] <interval> - interval in seconds when records will be exported
-m [--max-flows] <count> - maximum count of active flows
-t [--tcp-timeout] <seconds> - interval in seconds when TCP connection can be considered to be expired
"""

from pprint import pformat
from pprint import pprint as pp
import sys
from getopt import getopt, GetoptError
import traceback
import re
from struct import pack
import socket
from time import sleep
from copy import deepcopy
from tempfile import NamedTemporaryFile

import logging
# turn off annoying scappy message
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
# some basic configuration of debug messages
logging.basicConfig(format="%(levelname)s:\n%(message)s", level=logging.DEBUG)

import scapy.all as scapy

class ArgsParsingError(Exception):
    pass

_PROTOCOLS = {
    1: "icmp",
    2: "igmp",
    6: "tcp",
    17: "udp",
}

_FLAGS = {
    "A": "ACK",
    "C": "CWR",
    "E": "ECE",
    "F": "FIN",
    "P": "PSH",
    "R": "RST",
    "S": "SYN",
    "U": "URG",
}

class NetFlowExporter():
    def __init__(self, debug = False):
        # default values
        self.config = {
            "input": None,
            "collector_ip": "127.0.0.1",
            "collector_port": 2055,
            "interval": 300,
            "max_flows": 50,
            "tcp_timeout": 300
        }
        self.sys_starttime = 0
        self.expired_flows = []
        self.active_flows = {}
        self.exported_flows = 0
        # just for debug purposes
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.disabled = not debug

    def parse_args(self, args = sys.argv[1:]):
        if type(args) == str:
            args = args.split()
        try:
            parsed_args, unrecognized_args = getopt(args, "i:c:I:m:t:hb", ["input=", "collector=", "interval=", "max-flows=", "tcp-timeout=", "help"])
        except GetoptError as e:
            raise ArgsParsingError(e)
        if unrecognized_args:
            raise ArgsParsingError("unrecognized arguments: %s" % ", ".join(unrecognized_args))
        for arg, value in parsed_args:
            if arg in ["-b"]:
                print 12
                exit(0)
            if arg in ["-h", "--help"]:
                print _HELP
                exit(0)
            elif arg in ["-i", "--input"]:
                self.config.update({
                    "input": value
                })
            elif arg in ["-c", "--collector"]:
                if re.match(r"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}:[0-9]+$", value):
                    address, port = value.split(":")
                    for oct in address.split("."):
                        if not 0 <= int(oct) <= 255:
                            raise ArgsParsingError("not valid collector address: %s" % address)
                    if not 0 < int(port) <= 65535:
                        raise ArgsParsingError("not valid port: %s" % port)
                    self.config.update({
                        "collector_ip": address,
                        "collector_port": int(port),
                    })
                if re.match(r"^[\.-A-Za-z0-9]+:[0-9]+$", value):
                    hostname, port = value.split(":")
                    if not 0 < int(port) <= 65535:
                        raise ArgsParsingError("not valid port: %s" % port)
                    self.config.update({
                        "collector_ip": hostname,
                        "collector_port": int(port),
                    })
                elif re.match(r"^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$", value):
                    for oct in value.split("."):
                        if not 0 <= int(oct) <= 255:
                            raise ArgsParsingError("not valid collector address: %s" % value)
                    self.config.update({
                        "collector_ip": value
                    })
                if re.match(r"^[-A-Za-z0-9\\.]+$", value):
                    self.config.update({
                        "collector_ip": value,
                    })
                else:
                   raise ArgsParsingError("not valid collector address: %s" % value)
            elif arg in ["-I", "--interval"]:
                try:
                    value = int(value)
                except Exception as e:
                    raise ArgsParsingError("interval must be number")
                if not 0 < value:
                    raise ArgsParsingError("interval must be 0 <")
                self.config.update({
                    "interval": value
                })
            elif arg in ["-m", "--max-flows"]:
                try:
                    value = int(value)
                except Exception as e:
                    raise ArgsParsingError("max flows must be number")
                if not 0 < value:
                    raise ArgsParsingError("max flows must be 0 <")
                self.config.update({
                    "max_flows": value
                })
            elif arg in ["-t", "--tcp-timeout"]:
                try:
                    value = int(value)
                except Exception as e:
                    raise ArgsParsingError("tcp timeout must be number")
                if not 0 < value:
                    raise ArgsParsingError("tcp timeout must be 0 <")
                self.config.update({
                    "tcp_timeout": value
                })

    def export(self):
        very_first_packet = True
        data = None

        self.logger.debug("reading from file %s" % self.config["input"])


        if not self.config["input"]:
            # create temp file, problem with reading from stdin
            file = NamedTemporaryFile()
            file.write(sys.stdin.read())
            file.seek(0)
            filename = file.name
        else:
            filename = self.config["input"]

        for pkt in scapy.rdpcap(filename):

            # only IPv4 packets
            if not pkt.haslayer(scapy.IP):
                continue

            # save start time
            if very_first_packet:
                self.sys_starttime = pkt.time
                start_time = pkt.time
                very_first_packet = False
            
            if pkt.time - start_time > self.config["interval"]:
                start_time = pkt.time
                self.send_expired(pkt.time)

            # parse packet
            data = self.parse_packet(pkt)

            self.assign_to_flow(data)            
            self.check_expiration(data["time"])

        # send remaining
        for value in self.active_flows.values():
            self.expired_flows.append(value)
        self.send_expired(data["time"])

        if not self.config["input"]:
            file.close()

    def assign_to_flow(self, data):
        if _PROTOCOLS[data["protocol"]] == "tcp":
            if self.make_key(data) in self.active_flows:
                self.active_flows[self.make_key(data)]["last"] = data["time"]
                self.active_flows[self.make_key(data)]["num_of_packets"] += 1
                self.active_flows[self.make_key(data)]["layer3_bytes"] += data["layer3_bytes"]
                self.active_flows[self.make_key(data)]["tcp_flags"] |= data["tcp_flags"]["value"]
                self.active_flows[self.make_key(data)]["tcp_flags_list"].extend(data["tcp_flags"]["flags"])
            else:
                self.active_flows[self.make_key(data)] = {
                    "data": data,
                    "first": data["time"],
                    "last": data["time"],
                    "num_of_packets": 1,
                    "layer3_bytes": data["layer3_bytes"],
                    "protocol": _PROTOCOLS[data["protocol"]],
                    "tcp_flags": data["tcp_flags"]["value"],
                    "tcp_flags_list": []
                }
                self.active_flows[self.make_key(data)]["tcp_flags_list"].extend(data["tcp_flags"]["flags"])
        else:
            self.expired_flows.append({
                "data": data,
                "first": data["time"],
                "last": data["time"],
                "num_of_packets": 1,
                "layer3_bytes": data["layer3_bytes"],
                "protocol": _PROTOCOLS[data["protocol"]],
                "tcp_flags": 0,
            })

    def check_expiration(self, time):
        for key, data in sorted(self.active_flows.iteritems(), key = lambda (x, y): y["first"]):
            if "RST" in data["tcp_flags_list"][-1:] or ("FIN" in data["tcp_flags_list"][-2:] and "ACK" in data["tcp_flags_list"][-1:]):
                self.expired_flows.append(data)
                del self.active_flows[key]
                continue
            if time - data["last"] > self.config["tcp_timeout"]:
                self.expired_flows.append(data)
                del self.active_flows[key]
        if len(self.active_flows) > self.config["max_flows"]:
            key, data = sorted(self.active_flows.iteritems(), key = lambda (x, y): y["last"])[0]
            self.expired_flows.append(data)
            del self.active_flows[key]

    def make_key(self, data):
        return "%s_%s_%s_%s_%s" % (data["src_addr"], data["dst_addr"], data["src_port"], data["dst_port"], _PROTOCOLS[data["protocol"]])

    def send_expired(self, time):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        body = b""
        actual_flows = 0
        for data in sorted(self.expired_flows, key = lambda x: x["first"]):
            body += self.create_flow(data["data"], data["num_of_packets"], data["layer3_bytes"], data["first"], data["last"], data["tcp_flags"], pack = True)
            actual_flows += 1
            if actual_flows == 30:
                header = self.create_header(actual_flows, time, self.exported_flows, pack = True)
                datagram = header + body
                ret = sock.sendto(datagram, (self.config["collector_ip"], self.config["collector_port"]))
                self.logger.debug("send bytes %s" % ret)
                self.exported_flows += actual_flows
                actual_flows = 0
                body = b""
        # send remaining
        if body:
            header = self.create_header(actual_flows, time, self.exported_flows, pack = True)
            datagram = header + body
            ret = sock.sendto(datagram, (self.config["collector_ip"], self.config["collector_port"]))
            self.logger.debug("send bytes %s" % ret)
            self.exported_flows += actual_flows
        sock.close()

    def parse_packet(self, pkt):
        data = {
            "src_addr": pkt[scapy.IP].fields["src"],
            "dst_addr": pkt[scapy.IP].fields["dst"],
            "time": pkt.time,
            "layer3_bytes": len(pkt.getlayer(1)),
            "src_port": 0,
            "dst_port": 0,
            "protocol": pkt[scapy.IP].fields["proto"],
            "tcp_flags": 0,
            "tos": pkt[scapy.IP].fields["tos"],
        }
        if scapy.UDP in pkt[scapy.IP]:
            data.update({
                "src_port": pkt[scapy.IP][scapy.UDP].fields["sport"],
                "dst_port": pkt[scapy.IP][scapy.UDP].fields["dport"],
            })
        elif scapy.TCP in pkt[scapy.IP]:
            self.logger.debug(pformat(pkt[scapy.IP].fields))
            self.logger.debug(pformat(pkt[scapy.IP][scapy.TCP].fields))
            data.update({
                "src_port": pkt[scapy.IP][scapy.TCP].fields["sport"],
                "dst_port": pkt[scapy.IP][scapy.TCP].fields["dport"],
                "tcp_flags": {
                    "flags": [_FLAGS[x] for x in pkt.sprintf("%TCP.flags%")],
                    "value": pkt[scapy.IP][scapy.TCP].fields["flags"]
                }
            })
        elif scapy.ICMP in pkt[scapy.IP]:
            data.update({
                "dst_port": pkt[scapy.IP][scapy.ICMP].fields["type"] * 256 + pkt[scapy.IP][scapy.ICMP].fields["code"],
            })
        self.logger.debug(pformat(data))
        return data

    def create_flow(self, data, num_of_packets, layer3_bytes, first_packet_t, last_packet_t, tcp_flags = 0, pack = False):
        flow = {
            "srcaddr" : data["src_addr"],
            "dstaddr" : data["dst_addr"],
            "nexthop" : 0,
            "input" : 0,
            "output" : 0,
            "dPkts" : num_of_packets,
            "dOctets" : layer3_bytes,
            "first" : int((first_packet_t - self.sys_starttime) * 10**3),
            "last" : int((last_packet_t - self.sys_starttime) * 10**3),
            "srcport" : data["src_port"],
            "dstport" : data["dst_port"],
            "pad1" : 0,
            "tcp_flags" : tcp_flags,
            "prot" :  data["protocol"],
            "tos" : data["tos"],
            "src_as" : 0,
            "dst_as" : 0,
            "src_mask" : 0,
            "dst_mask" : 0,
            "pad2" : 0,
        }
        self.logger.debug(pformat(flow))
        return self.pack_flow(flow) if pack else flow 

    def pack_flow(self, flow):
        packed_flow = scapy.inet_aton(flow["srcaddr"]) + \
                      scapy.inet_aton(flow["dstaddr"]) + \
                      pack("!L", flow["nexthop"]) + \
                      pack("!H", flow["input"]) + \
                      pack("!H", flow["output"]) + \
                      pack("!L", flow["dPkts"]) + \
                      pack("!L", flow["dOctets"]) + \
                      pack("!L", flow["first"]) + \
                      pack("!L", flow["last"]) + \
                      pack("!H", flow["srcport"]) + \
                      pack("!H", flow["dstport"]) + \
                      pack("!B", flow["pad1"]) + \
                      pack("!B", flow["tcp_flags"]) + \
                      pack("!B", flow["prot"]) + \
                      pack("!B", flow["tos"]) + \
                      pack("!H", flow["src_as"]) + \
                      pack("!H", flow["dst_as"]) + \
                      pack("!B", flow["src_mask"]) + \
                      pack("!B", flow["dst_mask"]) + \
                      pack("!H", flow["pad2"])
        return packed_flow

    def create_header(self, num_of_flows, time, exported_flows, pack = False):
        header = {
            "version" : 5,
            "count" : num_of_flows,
            "sys_uptime" : int((time - self.sys_starttime) * 10**3), # in milliseconds
            "unix_secs" : int(time),
            "unix_nsecs" : int(time * 10**9) - int(time) * 10**9,
            "flow_sequence" : exported_flows,
            "engine_type" : 0,
            "engine_id" : 0,
            "sampling_interval" : 0
        }
        self.logger.debug(pformat(header))
        return self.pack_header(header) if pack else header

    def pack_header(self, header):
        packed_header = pack("!H", header["version"]) + \
                        pack("!H", header["count"]) + \
                        pack("!L", header["sys_uptime"]) + \
                        pack("!L", header["unix_secs"]) + \
                        pack("!L", header["unix_nsecs"]) + \
                        pack("!L", header["flow_sequence"]) + \
                        pack("!B", header["engine_type"]) + \
                        pack("!B", header["engine_id"]) + \
                        pack("!H", header["sampling_interval"])
        return packed_header

if __name__ == "__main__":
    try:
        exp = NetFlowExporter(debug = False)
        exp.parse_args()
        exp.export()
    except ArgsParsingError as e:
        print e
        print _HELP
        exit(1)
    except Exception as e:
        # print traceback.format_exc(e)
        print e
        print "Unexpected error"
        exit(99)